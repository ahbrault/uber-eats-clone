import firebase from "firebase";

const firebaseConfig = {
    apiKey: "AIzaSyAVXAo9tp-rb3XeBuMrw5Ci_sQw1ELa55s",
    authDomain: "rn-uber-eats.firebaseapp.com",
    projectId: "rn-uber-eats",
    storageBucket: "rn-uber-eats.appspot.com",
    messagingSenderId: "593669617701",
    appId: "1:593669617701:web:1681b44299875ee1e927cc"
};

!firebase.apps.length ? firebase.initializeApp(firebaseConfig) : firebase.app();

export default firebase;