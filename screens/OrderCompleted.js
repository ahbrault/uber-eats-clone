import React, {useEffect, useState} from 'react';
import {Text, View, StyleSheet, SafeAreaView, ScrollView} from 'react-native';
import {useSelector} from "react-redux";
import LottieView from "lottie-react-native"
import MenuItems from "../components/restaurantDetail/MenuItems";
import firebase from "../firebase";

const OrderCompleted = () => {
    const [lastOrder, setLastOrder] = useState({
        items: [
            {
                title: "Pizza Margherita",
                description: "Amazing pizza with fresh mozzarella and basilic",
                price: "$12.50",
                image: "https://fr.frije.com/content/recipes/120701/800-1.jpg"
            },
        ],
    });
    const {items, restaurantName} = useSelector(state => state.cartReducer.selectedItems);
    const total = items.map((item => Number(item.price.replace("$", "")))).reduce((prev, curr) => prev + curr, 0);
    const totalUSD = total.toLocaleString("en", {
        style: "currency",
        currency: "USD"
    });

    useEffect(() => {
        const db = firebase.firestore();
        const unsubscribe = db
            .collection("orders")
            .orderBy("createdAt", "desc")
            .limit(1)
            .onSnapshot((snapshot) => {
                snapshot.docs.map((doc) => {
                    setLastOrder(doc.data());
                });
            });
        return () => unsubscribe();
    }, [lastOrder]);

    return (
        <SafeAreaView style={styles.screen}>
            <View style={styles.container}>

                <LottieView style={styles.animationCheckmark}
                            source={require("../assets/animations/check-mark.json")}
                            autoPlay={true}
                            speed={.5}
                            loop={false}
                />
                <Text style={styles.orderText}>
                    Your order at {restaurantName} has been placed for {totalUSD}
                </Text>
                <ScrollView>
                    <MenuItems foods={lastOrder.items} hideCheckbox={true}/>

                    <LottieView style={styles.animationCooking}
                                source={require("../assets/animations/cooking.json")}
                                autoPlay={true}
                                speed={1}
                    />
                </ScrollView>
            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: "white"
    },
    container: {
        margin: 15,
        alignItems: "center",
        height: "100%"
    },
    animationCheckmark: {
        height: 100,
        alignSelf: "center",
        marginBottom: 30
    },
    animationCooking: {
        height: 200,
        alignSelf: "center"
    },
    orderText: {
        fontSize: 20,
        fontWeight: "bold",
        textAlign: "center"
    }
});

export default OrderCompleted;
