import React, { useState, useEffect } from 'react'
import { View, Text, SafeAreaView, ScrollView ,StyleSheet } from 'react-native'
import { Divider } from 'react-native-elements/dist/divider/Divider'
import BottomTabs from '../components/home/BottomTabs'
import Categories from '../components/home/Categories'
import HeaderTabs from '../components/home/HeaderTabs'
import RestaurantItems from '../components/home/RestaurantItems'
import SearchBar from '../components/home/SearchBar'

const YELP_API_KEY = "AQ3y6hzBjOKwM_Zt_lxpUDmmsLcUzPKzBoiWFrXv3CqEN-LrZM6F6XNmYx6jE00AI5EJ-Dulj5VauBQkPEVdgH3ZjnhJkqau6Zg-Dq9RRClkRbVZstM2vXqawKFHYXYx"

const localRestaurants = [
    {
        name: "Brasserie d'Auteil",
        image_url: "https://www.doitinparis.com/files/2017/sortir/weekend/04/thumbs-1180x525/auteuil-brasserie.jpg",
        categories: ["Pizza", "Salad"],
        price: "$$",
        reviews: 120,
        rating: 4.7
    },
    {
        name: "Mokus",
        image_url: "https://payload.cargocollective.com/1/5/177923/13879203/H6_905.jpg",
        categories: ["Pizza", "Pasta"],
        price: "$$",
        reviews: 120,
        rating: 4.3
    },
    {
        name: "Burger & Fries",
        image_url: "https://static.wixstatic.com/media/86e9ab_d61125d359514492a2baccadd90ceda7~mv2.jpg/v1/fill/w_640,h_414,al_c,q_80,usm_0.66_1.00_0.01/86e9ab_d61125d359514492a2baccadd90ceda7~mv2.webp",
        categories: ["Burger"],
        price: "$$",
        reviews: 120,
        rating: 4.5
    },
];

const Home = ({ navigation }) => {
    const [restaurantsData, setRestaurantsData] = useState(localRestaurants);
    const [city, setCity] = useState("Paris");
    const [activeTab, setActiveTab] = useState("Delivery");

    // Fetch Yelp API
    const getRestaurantsFromYelp = () => {
        const yelpUrl = `https://api.yelp.com/v3/businesses/search?term=restaurant&location=${city}`;
        const apiOptions = {
            headers: {
                Authorization: `Bearer ${YELP_API_KEY}`
            },
        };

        return fetch(yelpUrl, apiOptions)
            .then(res => res.json())
            .then(json => setRestaurantsData(json.businesses));
    };


    // Get restaurant list
    useEffect(() => {
          getRestaurantsFromYelp();
    }, [city])

    return (
        <SafeAreaView style={styles.screenContainer}>
            <View style={styles.header}>
                <HeaderTabs activeTab={activeTab} setActiveTab={setActiveTab} />
                <SearchBar cityHandler={setCity} />
            </View>
            <ScrollView showsVerticaScrollIndicator={false} style={styles.homeContent}>
                <Categories />
                <RestaurantItems restaurantsData={restaurantsData} navigation={navigation} />
            </ScrollView>
            <Divider width={1} />
            <BottomTabs />
        </SafeAreaView>
    )
};

const styles = StyleSheet.create({
    screenContainer:{
        flex: 1
    },
    header:{
        backgroundColor: "white",
        padding: 15
    },
    homeContent:{
        backgroundColor: "#eee"
    }
});

export default Home
