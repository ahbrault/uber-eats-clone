import React from 'react'
import {View, Text, Image, ScrollView} from 'react-native'
import {Divider} from 'react-native-elements'
import About from '../components/restaurantDetail/About'
import MenuItems from '../components/restaurantDetail/MenuItems'
import ViewCart from '../components/restaurantDetail/ViewCart'

// Food list
const foods = [
    {
        title: "Pizza Margherita",
        description: "Amazing pizza with fresh mozzarella and basilic",
        price: "$12.50",
        image: "https://fr.frije.com/content/recipes/120701/800-1.jpg"
    },
    {
        title: "Pizza Bufala",
        description: "Tasty pizza with mozzarella bufala",
        price: "$14.00",
        image: "https://galbani-professionale.fr/wp-content/uploads/2020/10/pizza-margherita-di-bufala-800x600-1.png"
    },
    {
        title: "Pizza Parma",
        description: "Tasty pizza with rocket salad, parma ham and permesan",
        price: "$16.20",
        image: "https://www.unileverfoodsolutions.ch/dam/global-ufs/mcos/dach/calcmenu-recipes/ch-recipes/2019/italien-promo/pizza-con-rucola-e-prosciutto-di-parma/main-header.jpg"
    },
    {
        title: "Pizza Tartuffo",
        description: "Amazing pizza with truffle and parisian mushroom",
        price: "$19.00",
        image: "https://i0.wp.com/www.lavespadeshalles.com/wp-content/uploads/2019/11/pizza-tartuffo1_ok.jpg?fit=4898%2C3265&ssl=1"
    },
];

const RestaurantDetail = ({route, navigation}) => {

    return (
        <View>
            <About route={route}/>
            <Divider width={1.8} style={{marginVertical: 20}}/>
            <MenuItems restaurantName={route.params.name} foods={foods}/>
            <ViewCart navigation={navigation}/>
        </View>
    )
}


export default RestaurantDetail
