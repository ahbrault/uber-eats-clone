import React from 'react'
import {View, Text, Image, ScrollView, StyleSheet} from 'react-native'
import {Divider} from 'react-native-elements';
import BouncyCheckbox from "react-native-bouncy-checkbox";
import {useDispatch, useSelector} from 'react-redux';

const MenuItems = ({restaurantName, foods, hideCheckbox, marginLeft}) => {
    const dispatch = useDispatch();

    // Use redux to add items to cart
    const selectedItem = (item, checkboxValue) => (
        dispatch({
            type: "ADD_TO_CART",
            payload: {
                ...item,
                restaurantName: restaurantName,
                checkboxValue: checkboxValue
            }
        })
    );

    // Select items form storage
    const cartItems = useSelector(
        (state) => state.cartReducer.selectedItems.items
    );

    // Get value of the selected item
    const isFoodInCart = (food, cartItems) => (
        Boolean(cartItems.find(item => item.title === food.title))
    );

    return (
        <ScrollView style={stylesMenuItems.listContainer}>
            {foods.map((food, index) => (
                <View key={index}>
                    <View style={stylesMenuItems.itemRow}>
                        {hideCheckbox ? <View/> : <BouncyCheckbox
                            iconStyle={stylesMenuItems.itemCheckbox}
                            fillColor="green"
                            onPress={(checkboxValue) => selectedItem(food, checkboxValue)}
                            isChecked={isFoodInCart(food, cartItems)}/>
                        }
                        <FoodInfo title={food.title} description={food.description} price={food.price}/>
                        <FoodImage image={food.image} marginLeft={marginLeft ? marginLeft : 0}/>
                    </View>
                    <Divider width={0.7} orientation="vertical" style={stylesMenuItems.divider}/>
                </View>
            ))}
        </ScrollView>
    );
}

const FoodInfo = ({title, description, price}) => (
    <View style={stylesFoodItem.infoContainer}>
        <Text style={stylesFoodItem.infoContent}>
            {title}
        </Text>
        <Text>{description}</Text>
        <Text>{price}</Text>
    </View>
);

const FoodImage = ({image, marginLeft}) => (
    <Image source={{uri: image}} style={{
        width: 100,
        height: 100,
        borderRadius: 10,
        marginLeft: marginLeft
    }}/>
);


const stylesMenuItems = StyleSheet.create({
    listContainer: {
        paddingBottom: 30
    },
    itemRow: {
        flexDirection: "row",
        justifyContent: "space-between",
        margin: 20
    },
    itemCheckbox: {
        borderColor: "lightgrey",
        borderRadius: 3
    },
    divider: {
        marginHorizontal: 20
    }
});

const stylesFoodItem = StyleSheet.create({
    infoContainer: {
        width: 240,
        justifyContent: "space-evenly"
    },
    infoContent: {
        fontSize: 19,
        fontWeight: "600"
    }
});

export default MenuItems

