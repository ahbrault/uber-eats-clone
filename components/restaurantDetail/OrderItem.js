import React from 'react'
import {View, Text, StyleSheet} from 'react-native'

const OrderItem = ({item}) => {
    const {title, price} = item;

    return (
        <View style={styles.itemsContainer}>
            <Text style={styles.itemName}>
                {title}
            </Text>
            <Text style={styles.itemPrice}>
                {price}
            </Text>
        </View>
    )
};

const styles = StyleSheet.create({
    itemsContainer: {
        flexDirection: "row",
        justifyContent: "space-between",
        padding: 20,
        borderBottomWidth: 1,
        borderBottomColor: "#999"
    },
    itemName: {
        fontWeight: "600",
        fontSize: 16
    },
    itemPrice: {
        opacity: .7,
        fontSize: 16
    }
})

export default OrderItem
