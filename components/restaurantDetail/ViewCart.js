import React, {useState} from 'react'
import {View, Text, TouchableOpacity, Modal, StyleSheet} from 'react-native'
import {useSelector} from 'react-redux'
import OrderItem from "./OrderItem";
import firebase from "../../firebase"
import LottieView from "lottie-react-native"

const ViewCart = ({navigation}) => {
    const [modalVisible, setModalVisible] = useState(false);
    const [loading, setLoading] = useState(false);

    const {items, restaurantName} = useSelector(state => state.cartReducer.selectedItems);
    const total = items.map((item => Number(item.price.replace("$", "")))).reduce((prev, curr) => prev + curr, 0);
    const totalUSD = total.toLocaleString("en", {
        style: "currency",
        currency: "USD"
    });

    // Send order to db
    const addOrderToFireBase = () => {
        setLoading(true);
        const db = firebase.firestore();
        db.collection("orders")
            .add({
                items: items,
                restaurantName: restaurantName,
                createdAt: firebase.firestore.FieldValue.serverTimestamp(),
            })
            .then(() => {
                setTimeout(() => {
                    setLoading(false);
                    navigation.navigate("OrderCompleted");
                }, 2500);
            });
    };

    // Return the checkout modal
    const checkoutModalContent = () => (
        <View style={stylesModal.modalContainer}>
            <View style={stylesModal.checkoutContainer}>
                <Text style={stylesModal.restaurantName}>{restaurantName}</Text>
                {items.map((item, index) => (
                    <OrderItem key={index} item={item}/>
                ))}
                <View style={stylesModal.subTotalContainer}>
                    <Text style={stylesModal.subTotalText}>Subtotal</Text>
                    <Text>{totalUSD}</Text>
                </View>
                <View style={stylesModal.buttonContainer}>
                    <TouchableOpacity style={stylesModal.button}
                                      onPress={() => {
                                          addOrderToFireBase();
                                          setModalVisible(false);
                                      }}>
                        <Text style={stylesModal.buttonText}>Checkout</Text>
                        <Text style={stylesModal.buttonSubTotal}>{total ? totalUSD : ""}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );

    return (

        <>
            <Modal
                animationType="slide"
                visible={modalVisible}
                transparent={true}
                onRequestClose={() => setModalVisible(false)}
            >
                {checkoutModalContent()}
            </Modal>
            {total ? (
                <View
                    style={{
                        flex: 1,
                        alignItems: "center",
                        justifyContent: "center",
                        flexDirection: "row",
                        position: "absolute",
                        bottom: 130,
                        zIndex: 999,
                    }}
                >
                    <View
                        style={{
                            flexDirection: "row",
                            justifyContent: "center",
                            width: "100%",
                        }}
                    >
                        <TouchableOpacity
                            style={{
                                marginTop: 20,
                                backgroundColor: "black",
                                flexDirection: "row",
                                justifyContent: "flex-end",
                                padding: 15,
                                borderRadius: 30,
                                width: 300,
                                position: "relative",
                            }}
                            onPress={() => setModalVisible(true)}
                        >
                            <Text style={{ color: "white", fontSize: 20, marginRight: 30 }}>
                                View Cart
                            </Text>
                            <Text style={{ color: "white", fontSize: 20 }}>{totalUSD}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            ) : (
                <></>
            )}
            {loading ? (
                <View
                    style={{
                        backgroundColor: "black",
                        position: "absolute",
                        opacity: 0.6,
                        justifyContent: "center",
                        alignItems: "center",
                        height: "100%",
                        width: "100%",
                    }}
                >
                    <LottieView
                        style={{ height: 200 }}
                        source={require("../../assets/animations/scanner.json")}
                        autoPlay
                        speed={3}
                    />
                </View>
            ) : (
                <></>
            )}
        </>
    );
};

const stylesModal = StyleSheet.create({
    modalContainer: {
        flex: 1,
        justifyContent: 'flex-end',
        backgroundColor: "rgba(0,0,0,.7)",
    },
    checkoutContainer: {
        backgroundColor: "white",
        padding: 16,
        height: 500,
        borderWidth: 1
    },
    restaurantName: {
        textAlign: "center",
        fontWeight: "600",
        fontSize: 18,
        marginBottom: 10
    },
    subTotalContainer: {
        flexDirection: "row",
        justifyContent: "space-between",
        marginTop: 15
    },
    subTotalText: {
        textAlign: "left",
        fontWeight: "600",
        fontSize: 15,
        marginBottom: 10
    },
    buttonContainer: {
        flexDirection: "row",
        justifyContent: "center"
    },
    button: {
        marginTop: 20,
        backgroundColor: "black",
        alignItems: "center",
        padding: 13,
        borderRadius: 30,
        width: 300,
        position: "relative"
    },
    buttonText: {
        color: "white",
        fontSize: 20,
    },
    buttonSubTotal: {
        position: "absolute",
        right: 20,
        color: "white",
        fontSize: 15,
        top: 17
    }
});

const stylesViewCart = StyleSheet.create({
    buttonContainer: {
        flex: 1,
        alignItems: "center",
        flexDirection: "row",
        position: "absolute",
        bottom: 50,
        zIndex: 999
    },
    button: {
        flexDirection: "row",
        justifyContent: "center",
        width: "100%",
    },
    buttonContent: {
        marginTop: 20,
        backgroundColor: "black",
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "center",
        padding: 15,
        borderRadius: 30,
        width: 300,
        position: "relative"
    },
    buttonText: {
        color: "white",
        fontSize: 20
    },
    buttonSubTotal: {
        fontSize: 20,
        color: "white",
        position: "absolute",
        right: 20
    }
});


export default ViewCart
