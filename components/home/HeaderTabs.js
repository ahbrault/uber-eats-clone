import React, { useState } from 'react'
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native'

const HeaderTabs = ({ activeTab, setActiveTab }) => {
    return (
        <View style={styles.headerTabs}>
            <HeaderButton
                text="Delivery"
                activeTab={activeTab}
                setActiveTab={setActiveTab} />

            <HeaderButton
                text="Pickup"
                activeTab={activeTab}
                setActiveTab={setActiveTab} />
        </View>
    );
};

const HeaderButton = ({ text, activeTab, setActiveTab }) => {
    return (
        <TouchableOpacity
            style={{
                backgroundColor: activeTab === text ? "black" : "white",
                paddingVertical: 6,
                paddingHorizontal: 16,
                borderRadius: 30
            }}
            onPress={() => setActiveTab(text)}>
            <Text
                style={{
                    color: activeTab === text ? "white" : "black",
                    fontSize: 15,
                    fontWeight: "900"
                }}>
                {text}
            </Text>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    headerTabs:{
        flexDirection: "row",
        alignSelf: "center"
    }
});

export default HeaderTabs
