import React from 'react'
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native'
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import Ionicons from "react-native-vector-icons/Ionicons";
import AntDesign from "react-native-vector-icons/AntDesign";

const SearchBar = ({cityHandler}) => {
    return (
        <View style={styles.inputContainer}>
            <GooglePlacesAutocomplete
                query={{key: "AIzaSyAxrLe6ueacQ7Osid0FSq5iDWt_KfNU9no"}}
                onPress={(data, details = null) => {
                    const city = data.description.split(',')[0]
                    cityHandler(city)
                }}
                placeholder="Search"
                styles={{
                    textInput: {
                        backgroundColor: "#eee",
                        borderRadius: 20,
                        fontWeight: "700",
                        marginTop: 7
                    },
                    textInputContainer: {
                        backgroundColor: "#eee",
                        borderRadius: 50,
                        flexDirection: "row",
                        alignItems: "center",
                        marginRight: 10
                    }
                }}
                renderLeftButton={() => (
                    <View style={styles.searchIcon}>
                        <Ionicons name="location-sharp" size={24}/>
                    </View>
                )}
                renderRightButton={() => (
                    <TouchableOpacity style={styles.inputButton}>
                        <AntDesign name="clockcircle" size={11} style={styles.buttonIcon}/>
                        <Text>Search</Text>
                    </TouchableOpacity>
                )}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    inputContainer: {
        marginTop: 15,
        flexDirection: "row"
    },
    searchIcon: {
        marginLeft: 10
    },
    inputButton: {
        flexDirection: "row",
        alignItems: "center",
        backgroundColor: "white",
        padding: 10,
        borderRadius: 20,
        marginRight: 8,
    },
    buttonIcon: {
        marginRight: 8
    }
});

export default SearchBar
