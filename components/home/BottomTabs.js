import React from 'react'
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native'
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";

const BottomTabs = () => {
    return (
        <View style={styles.bottomTabs}>
            <Icon icon="home" text="Home" />
            <Icon icon="search" text="Browse" />
            <Icon icon="shopping-bag" text="Grocery" />
            <Icon icon="receipt" text="Orders" />
            <Icon icon="user" text="Account" />
        </View>
    )
};

const Icon = ({ icon, text }) => (
    <TouchableOpacity>
        <FontAwesome5 name={icon} size={25} style={styles.icon} />
        <Text>{text}</Text>
    </TouchableOpacity>
);

const styles = StyleSheet.create({
    bottomTabs:{
        flexDirection: "row",
        justifyContent: "space-between",
        padding: 10,
        paddingHorizontal: 30,
    },
    icon:{
        marginBottom: 3,
        alignSelf: "center",
    }
})


export default BottomTabs
