import React from 'react'
import {View, Text, Image, ScrollView, StyleSheet} from 'react-native'

// Categories name & image
const items = [
    {
        image: require("../../assets/images/shopping-bag.png"),
        text: "Pick-up"
    },
    {
        image: require("../../assets/images/bread.png"),
        text: "Backery Items"
    },
    {
        image: require("../../assets/images/fast-food.png"),
        text: "Fast Foods"
    },
    {
        image: require("../../assets/images/deals.png"),
        text: "Deals"
    },
    {
        image: require("../../assets/images/coffee.png"),
        text: "Coffee & Tea"
    },
    {
        image: require("../../assets/images/desserts.png"),
        text: "Desserts"
    }
];

const Categories = () => {
    return (
        <View style={styles.categories}>
            <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                {items.map((item, index) => (
                    <View style={styles.itemContainer} key={index}>
                        <Image
                            source={item.image}
                            style={styles.itemImage}/>
                        <Text style={styles.itemText}>
                            {item.text}
                        </Text>
                    </View>
                ))}
            </ScrollView>
        </View>
    )
};

const styles = StyleSheet.create({
    categories: {
        backgroundColor: "white",
        paddingVertical: 10,
        paddingLeft: 20
    },
    itemContainer: {
        alignItems: "center",
        marginRight: 30
    },
    itemImage: {
        width: 50,
        height: 40,
        resizeMode: "contain"
    },
    itemText: {
        fontSize: 13,
        fontWeight: "900"
    }
})


export default Categories
