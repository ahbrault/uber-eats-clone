import React from 'react'
import { View, Text, Image, TouchableOpacity, StyleSheet } from 'react-native'
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

const RestaurantItems = ({ navigation, restaurantsData }) => {
    return (
        restaurantsData.map((restaurant, index) => (
            <TouchableOpacity key={index} activeOpacity={1}
                onPress={() => {
                    navigation.navigate("RestaurantDetail", {
                        name: restaurant.name,
                        image: restaurant.image_url,
                        price: restaurant.price,
                        reviews: restaurant.review_count,
                        rating: restaurant.rating,
                        categories: restaurant.categories,
                        restaurant: restaurant
                    })
                }}>
                <View style={styles.restaurantContainer}>
                    <RestaurantImage image={restaurant.image_url} />
                    <RestaurantInfo name={restaurant.name} rating={restaurant.rating} />
                </View>
            </TouchableOpacity>
        ))
    )
}

const RestaurantImage = ({ image }) => {
    return (
        <View>
            <Image
                source={{ uri: image }}
                style={styles.restaurantImage} />
            <TouchableOpacity style={styles.likeButton}>
                <MaterialCommunityIcons name="heart-outline" size={25} color="white" />
            </TouchableOpacity>
        </View>
    )
}

const RestaurantInfo = ({ name, rating }) => {
    return (
        <View style={styles.restaurantInfo}>
            <View>
                <Text style={styles.restaurantName}>{name}</Text>
                <Text style={styles.restaurantTime}>30-45 min</Text>
            </View>
            <View style={styles.restaurantRating}>
                <Text style={styles.ratingText}>
                    {rating}
                </Text>
            </View>
        </View>
    )
};

const styles = StyleSheet.create({
    restaurantContainer:{
        marginTop: 10,
        backgroundColor: "white",
        padding: 15
    },
    restaurantImage:{
        width: "100%",
        height: 180
    },
    likeButton:{
        position: "absolute",
        right: 10,
        top: 10
    },
    restaurantInfo:{
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        marginTop: 10
    },
    restaurantName:{
        fontSize: 15,
        fontWeight: "bold"
    },
    restaurantTime:{
        fontSize: 13,
        color: "grey"
    },
    restaurantRating:{
        backgroundColor: "#eee",
        height: 30,
        width: 30,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 15
    },
    ratingText:{
        fontSize: 11
    }
});

export default RestaurantItems
